
import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
//import '~antd/dist/antd.css';
import user from './logo.png';
import './App.css';
import { Drawer, List, Avatar, Statistic, Row, Col, Progress, Layout, Menu, Form, Icon, Input, Button, Checkbox, Table, InputNumber, Popconfirm, Select, Spin } from 'antd';
import debounce from 'lodash/debounce';
//const { Form, Icon, Input, Button, Checkbox, Table, InputNumber, Popconfirm } = antd;

//const { Table, Input, Button, Popconfirm, Form } = antd;


//const { Layout, Menu, Icon } = antd;

const { Header, Sider, Content } = Layout;

const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
  state = {
    editing: false,
  };

  toggleEdit = () => {
    const editing = !this.state.editing;
    this.setState({ editing }, () => {
      if (editing) {
        this.input.focus();
      }
    });
  };

  save = e => {
    const { record, handleSave } = this.props;
    this.form.validateFields((error, values) => {
      if (error && error[e.currentTarget.id]) {
        return;
      }
      this.toggleEdit();
      handleSave({ ...record, ...values });
    });
  };

  renderCell = form => {
    this.form = form;
    const { children, dataIndex, record, title } = this.props;
    const { editing } = this.state;
    return editing ? (
      <Form.Item style={{ margin: 0 }}>
        {form.getFieldDecorator(dataIndex, {
          rules: [
            {
              required: true,
              message: `${title} is required.`,
            },
          ],
          initialValue: record[dataIndex],
        })(<Input ref={node => (this.input = node)} onPressEnter={this.save} onBlur={this.save} />)}
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{ paddingRight: 24 }}
        onClick={this.toggleEdit}
      >
        {children}
      </div>
    );
  };

  render() {
    const {
      editable,
      dataIndex,
      title,
      record,
      index,
      handleSave,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editable ? (
          <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
        ) : (
          children
        )}
      </td>
    );
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'Project Name',
        dataIndex: 'projectName',
        width: '15%',
        editable: true,
      },
      {
        title: 'Client Name',
        dataIndex: 'clientName',
        width: '15%',
        editable: true,
      },
      {
        title: 'Developer',
        dataIndex: 'developer',
        width: '15%',
        editable: true,
      },
      {
        title: 'Date Started',
        dataIndex: 'dateStarted',
        width: '15%',
        editable: true,
      },
      {
        title: 'Date Completion',
        dataIndex: 'dateCompletion',
        width: '15%',
        editable: true,
      },
      {
        title: 'operation',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];

    this.state = {
      dataSource: [
        {
          key: '0',
          projectName: 'ZDRC Website',
          clientName: 'ZDRC / Miss Kim',
          developer: 'Insert Name Here',
          dateStarted: '08/29/2019',
          dateCompletion: '12/12/2019',
        },
        {
          key: '1',
          projectName: 'ZDRC Website',
          clientName: 'ZDRC / Miss Kim',
          developer: 'Insert Name Here',
          dateStarted: '08/29/2019',
          dateCompletion: '12/12/2019',
        },
      ],
      count: 2,
    };
  }

  handleDelete = key => {
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
  };

  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newData = {
      key: count,
      fname: `Insert first NAme hered`,
      lname: `Insert first NAme hered`,
      age: 32,
      address: `Insert address here`,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };

  handleSave = row => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  };

  render() {
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <div>
        <Button onClick={this.handleAdd} type="primary" style={{ marginBottom: 16 }}>
          Add Project
        </Button>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
      </div>
    );
  }
}


class ProgressBar extends React.Component{
  render() {
    return (
      <div>
        <ul id="projectList">
          <li>
            <Progress type="circle" percent={75} format={percent => `${percent}%`} />
            <List.Item.Meta
                avatar={
                  <Avatar src="https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png" />
                }
                title={<a href="https://ant.design/index-cn">Insert Project Name</a>}
                description="Insert Dev Name"
              />
          </li>
          <li>
            <Progress type="circle" percent={5} format={percent => `${percent}%`} />
            <List.Item.Meta
                avatar={
                  <Avatar src="https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png" />
                }
                title={<a href="https://ant.design/index-cn">Insert Project Name</a>}
                description="Insert Dev Name"
              />
          </li>
          <li>
            <Progress type="circle" percent={100} format={percent => `${percent}%`} />
            <List.Item.Meta
                avatar={
                  <Avatar src="https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png" />
                }
                title={<a href="https://ant.design/index-cn">Insert Project Name</a>}
                description="Insert Dev Name"
              />
          </li>
          <li>
            <Progress type="circle" percent={100} format={percent => `${percent}%`} />
            <List.Item.Meta
                avatar={
                  <Avatar src="https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png" />
                }
                title={<a href="https://ant.design/index-cn">Insert Project Name</a>}
                description="Insert Dev Name"
              />
          </li>
        </ul>
      </div>
    );
  }
}

class StatisticBar extends React.Component{
  render(){
    return(
      <Row gutter={16}>
        <Col span={6} id="projNum">
          <Statistic title="Total Number of Projects" value={128} prefix={<Icon type="project" />} />
        </Col>
        <Col span={6} id="done">
          <Statistic title="Done" prefix={<Icon type="heart-o" />} value={30} suffix="" />
        </Col>
        <Col span={6} id="inProg">
              <Statistic title="In Progress" prefix={<Icon type="hourglass" />} value={7} suffix="" />
            </Col>
        <Col span={6} id="pending">
              <Statistic title="Pending" prefix={<Icon type="paper-clip" />} value={0} suffix="" />
            </Col>
      </Row>
    );
  }
}




class SiderDemo extends React.Component {

  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout style={{ height: '100vh' }}>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <img src={user} className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">
              <Icon type="dashboard" />
              <span>Dashboard</span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="idcard" />
              <span>Admin</span>
            </Menu.Item>
            <Menu.Item key="3">
              <Icon type="user" />
              <span>User</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 280,
            }}
          >
          <StatisticBar/><br/>
          <ProgressBar/><br/>
          <EditableTable/>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

ReactDOM.render(<SiderDemo />, document.getElementById("container"));

export default SiderDemo;